# Developers note:

This is a custom group of files extracted from the custom built fontawesome subset made with the Font Awesome Pro subsetter App using the injusta_subset.yaml file included here.

Only files required for this theme configuration and built practices are included here, but the Fontawesome Subset can provide much more files that the ones present. If required, rebuild the subset by loading the injusta_subset.yaml in the App.


# Font Awesome 5.15.4

Thanks for downloading Font Awesome! We're so excited you're here.

Our documentation is available online. Just head here:

https://fontawesome.com
