(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.slickCarousels = {
    attach: function (context, settings) {

      var $slickCarouselsMultiple = $('.slick-carousel--multiple', context);
      
      if($slickCarouselsMultiple.length) {
        $slickCarouselsMultiple.slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }

      var $slickCarouselsSingle = $('.slick-carousel--single', context);

      if($slickCarouselsSingle.length) {
        $slickCarouselsSingle.slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);