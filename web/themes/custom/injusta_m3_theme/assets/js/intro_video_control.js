(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.introVideo = {
    attach: function (context, settings) {
      var $introVideoModal = $(".homepage__video-modal", context);
      var $modalLaunchButton = $(".homepage__video-button", context);
      var $modalCloseButton = $introVideoModal.find(".close");
      var $modalIframe = $introVideoModal.find("iframe").first();
      window.cookieName = "introVideoHide";

      if($introVideoModal.length > 0 && $modalLaunchButton.length > 0) {
        if(!$.cookie('introVideoHide')) {
          $modalLaunchButton.click();
        }

        $modalCloseButton.click(function(ev){
          $.cookie('introVideoHide', true);
          var el_src = $modalIframe.attr("src");
          $modalIframe.attr("src",el_src);
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);