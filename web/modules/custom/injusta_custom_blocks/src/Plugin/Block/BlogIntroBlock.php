<?php

namespace Drupal\injusta_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'BlogIntro' Block.
 *
 * @Block(
 *   id = "blog_intro_block",
 *   admin_label = @Translation("Blog introduction text"),
 *   category = @Translation("Injusta custom blocks"),
 * )
 */
class BlogIntroBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'blogintro';
    return array(
      '#theme' => 'freeform',
      '#code' => $code
    );
  }

}
