<?php

namespace Drupal\injusta_custom_blocks\Form;

use \Drupal;
use \Exception;
use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;

class InjustaContactUs extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#prefix'] = '<div id="contact-us-form-wrapper">';
    $form['#suffix'] = '</div>';

    $form['fields'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'fields-container',
      ]
    ];

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'actions-container',
      ]
    ];

    $fields = &$form['fields'];
    $actions = &$form['actions'];

    $fields['survey'] = [
      '#type' => 'checkboxes',
      '#options' => array(
        'seguimiento' => $this->t('Suscribirme a un caso'),
        'colaboracion' => $this->t('Sumarme al proyecto'),
        'invitacion' => $this->t('Invitarlas a dar una charla'),
        'sitio' => $this->t('Comentar sobre el sitio'),
        'blog' => $this->t('Escribir para el blog: Más allá de lo penal')
      ),
      '#title' => $this->t('Me interesa...'),
      '#required' => TRUE
    ];

    $fields['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE
    ];

    $fields['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE
    ];

    $fields['organization'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organization'),
      '#required' => TRUE
    ];

    $fields['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE
    ];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  public function getFormId() {
    return 'injusta_custom_blocks_contact_form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    return $form;
  }
}

