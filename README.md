# Matrushka Standard CMS (M-III) based project for Balance

## Using Lando
Lando is recommended for the development of Drupal 8 based projects. 

### Initialize lando virtual server
First make sure Docker manager is up and runing. Then go to the root path of this repo in your console and run `lando start` to initialize a virtual server instance following the settings configured in `.lando.yml` file, wich are by default a standard drupal 8 recipe.


### Installing Drupal Core and dependencies
To install Drupal core files and all of its dependencies run `lando composer install` in the root path of this repo.

If the virtual server is not running, this will first launch it then it will proceed to run composer install on the server instance.


### Configuring and Installing database
Using the web-browser visit the url path generated for the lando virtual server (displayed on ypur console), the drupal 8 instalation page should be displayed. You won't be prompted to choose language or profile instalation, but it is required to set up default lando database configuration settings as follows:

~~~~~
database: drupal8
username: drupal8
password: drupal8
host: database
port: 3306
~~~~~

For more information about Lando virtual database server configuration in drupal instances visit this [link](https://docs.devwithlando.io/tutorials/drupal8.html#connecting-to-your-database).

### Installing new Drupal modules
It is recommended to manage new required Drupal modules with composer. In order to register a new drupal module in the `composer.json` file of the root of your project, use the following command:

~~~~~
lando composer require "drupal/module_name"
~~~~~

That will download module files under `modules/contrib` and update `composer.json` with the new module name as a requirement. New modules should be enabled later with *drush* (`drush en module_name`) or using the web admin UI.

It is no recommended to include these module files in new commits, only updates to `composer.json` file and module configurations exported to *sync folder* should be included in your project repository.

### Including new Drupal modules in M-III
If the new module is going to be included in the M-III installation profile, it should be added to the dependency list inside the `composer.json` file of this repository and declared in the install module list in the profile `info.yml` file.